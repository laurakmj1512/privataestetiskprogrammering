var stars;

let rectW = 120;
let rectH = 20;
let rectX;
let rectY;
let brickW = 80 ;
let brickH = 30;

//et array for murstenene
let bricks = [];

function preload() {
  font = loadFont("kongtext.ttf");
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  
  stars = createGraphics (displayWidth, displayHeight);
    //der sættes en grænse for hvor mange gange den skal loope - hvor mange stjerne der skal laves - 1000 stjerner
    for (let i = 0; i < 700; i++) {
      stars.fill(255,random(250));
      stars.noStroke();
      stars.ellipse(random(width), random(height), random(2, 5));
    }
  
  //ball laves
  ball = new Ball();
  for (var _x =0; _x < width/brickW; _x++) {
    for (var _y = 0; _y < (height/2) / brickH; _y++) {
      bricks[_x] = bricks[_x] || [];
      bricks[_x][_y] = new Brick(_x, _y, brickW, brickH);
    }
  }


  
}



function draw() {
  background(0, 10, 20);
  image(stars, 0, 0);
  
  //battet (rect) laves
  drawRect();

  //bevægelsen af ball laves 
  ball.display();
  ball.move();
  ball.bounce();
 

  //brick bliver lavet
  for (var _x = 0; _x < width/brickW; _x++) {
    for (var _y = 0; _y < (height/2) / brickH; _y++) {
      if (dist(ball.x, ball.y, bricks[_x][_y].x, bricks[_x][_y].y) < 60) {
        bricks[_x][_y].show = false;
        //bricks.splice([_x][_y], 1);
        console.log("hej");
        ball.dobounce();
        
      }
      bricks[_x][_y].display();
    }
  }
  //hvis ball tabes - altså er højere en height - taber man
  if (ball.y > height) {
    gameOver();
  }
}

//battet du bruger til ramme bolden med laves 
function drawRect(){
  rectX = mouseX;
  rectY = height-rectH;
  fill(random(0, 255), random(0, 255), random(0, 255));
  noStroke();
  rect(rectX, rectY, rectW, rectH);
}

function drawBrick() {
  for (x = 0; x < width; x = x + 30) {
    for (y = 0; y < width/2; y = y + 15) {
      fill(random(0, 255), random(0, 255),random(0, 255));
      rect(x, y, brickW, brickH);
    }
  }
}

function gameOver() {
  background(0, 10, 20, 220);
  fill(255, 0, 0);
  noStroke();
  textFont(font);
  textSize(150);
  textAlign(CENTER);
  text("DEAD", width/2, height/2);
}


//REFERENCELISTE

//Font:
//https://www.1001fonts.com/kongtext-font.html
