
class Brick{
    constructor(_x,_y, _w, _h){
      this.x=_x * _w;
      this.y=_y * _h;
      this.w=_w;
      this.h=_h;
      this.fill= [random(0,255), random(0,255), random(0,255)];
      this.show = true;
    }
  
    display(){
      if (this.show) {
        stroke(0, 10, 20);
        strokeWeight(8);
        fill.apply(fill, this.fill);
        rect(this.x,this.y,this.w,this.h);
      }
    }
  }

  
class Ball {
  constructor() {
    this.x = random(rectX, rectX + rectW);
    this.y =(height/2, height - rectH);
    this.r = 20;
    this.speedX = 4;
    this.speedY = 4;
    this.active = true;
  }

  display() {
    fill(0, 0, 255);
    noStroke();
    ellipse(this.x, this.y, this.r, this.r);
  }

move() {
  this.x = this.x - this.speedX;
  this.y = this.y - this.speedY; 
}

bounce() {
  if (this.x <=0 || this.x >=width) {
    this.speedX = this.speedX * (-1);
  } 
  if (this.y < height/2 || (this.y >= (height - rectH) && this.x > rectX && this.x < (rectX + rectW))) {
    this.speedY = this.speedY * (-1);
  } 
}

dobounce() {
    this.speedY = this.speedY * (-1);
}

} 






