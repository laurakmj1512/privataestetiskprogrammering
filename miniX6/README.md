# MiniX6: Object abstraction

_Link til min [RunMe](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX6/index.html)_

_Link til mit kode:_ 
- _[sketch.js](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX6/sketch.js)_
- _[bricks_ball.js](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX6/bricks_ball.js)_

<!-- blank line -->
----
<!-- blank line -->

Describe how does/do your game/game objects work?
Jeg fandt dette miniX emne ret svært. Jeg forstår nogenlunde konceptet men jeg har stadig svært ved at komme på kode selv så fik en del hjælp og inspiration fra 
[p5.js](https://editor.p5js.org/Huiyi/sketches/BkaSwMVTW). Jeg ville rigtig skabe en spil med et retro touch. Forsøgte først at lave [Minesweeper](https://minesweeperonline.com/). For dem der ikke kender det er det et spil hvor man har et gitter med kvadraterder gemmer på miner.

![ALT](https://minesweeper.online/img/homepage/intermediate.png)

Spillet går ud på at at fjerne kvadrater uden at finde minerne hvilket kan gøres ved hjælp af tallene. Hvis der står `1` betyder det at der i felterne omkring tallet befinder sig en mine, hvis tallet er `2` befinder der sig en mine i 2 af felterne omkring tallet osv. Desværre kunne jeg ikke få det til at fungere. Planen er dog forhåbentlig at lave det som min miniX7 hvor vi skal forbedre en af vores tidligere miniX'er. 

Efter flere dage med frustrende kode der ikke virkede valgte jeg derfor at gå videre til et andet klassisk spil: Brick Breaker. Spillet og min kode fungere således at man skal ødelægge bricks ved hjælp af en ball man ikke må tabe. Man rammer sin ball med et bat man styre med sin mus. 

![](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX6/miniX6_videoEks.mov)

Visuelt gik jeg virkelig efter en form for retro vibe og meget farvefyld vibe. Mine bricks er random farvelagt og battet man bruger til at ramme ens ball med har en random farveudfyldning der skifter konstant. Hvis jeg skal være helt ærlig ved jeg ikke hvorfor den gør det men det sser stadig cool ud og passer til temaet. Når man dør kommer der med en retro gamin font store fede røde bogstaver der melder at man er død og har tabt spillet.

![](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX6/webside_screenshot.png)

![](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX6/websideDead_screenshot.png)

<!-- blank line -->
----
<!-- blank line -->

I forhold til emnet object abstraction passer ugens pensum godt med dette spil da koden indeholder en del objects som har forskellige behaviors som vi kan indele og skabe ordne blandt via forskellige classes. 

I min kode anvender jeg to classes som er `class Brick`og `class Ball`. Hver class er defineret i min [bricks_ball.js](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX6/bricks_ball.js) fil. 

Forneden kan der ses min `class Brick`i min kode. Via `constructor()` definere jeg visse forhold som kun er gældende for denne bestemte class `this.x`og `this.y`definere mine bricks positioner, `this.w`og `this.h`definere deres højde og bredde, `this.fill`defineres deres bagrundsfarve og `this.show`definere sat det skal visuelt vises. 

![](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX6/kodeClass_screenshot.png)

Derefter definere jeg i min class hvilke parameter der er gældende hvis mine bricks vises. Deres `stroke()`, `strokeWeight(`), `fill.apply()` og selve dannelen af `rect()`.

Til trods for at jeg til forskel for min Minesweeper drømme og jeg faktisk fik dette til at fungere(ish) er jeg ikke selv tilfreds med det. Jeg kan få mine bricks frem, mit bat nede i bunden frem, min ball til at bevæge sig mellem mine bricksog min bat men når det kommer til efter mine bricks er ramt er der problemer. 

Som det kan ses på billedet og i min video tidligere forsvinder min bricks når min ball befinder sig i en bricks position. Men de forsvinder ikke fuldstændig men vises blot ikke. Dette betyder at min ball ikke kan komme op til de bricks der befinder sig i den næste række. og derved forsvinder hele formålet lidt med spillet for man kan ikke rigitg vinde.

I min kode fjerne jeg mine bricks efter de er blevet ramt af ens ball ved at skrive `bricks[x_][y_].show = false`. Hvis ens ball befinder sig i en bestemt bricks position bliver show false og derfor skal den ikke vises. Det fungere også men problemet er at den ikke forsvinder. For at få mine bricks til at forsvinde fuldstændig forsøgte jeg med `splice()`. `splice()` er en kommando som ændre ens indhold i ens array ved at enten udskifte/fjerne eksisterende elementer. I billedet forneden kan der ses at jeg har udkommenteret `splice()`men ved at gøre den gældende fjerne den ikke en specifik brick men derimod en hel kolonne... 

Som det kan se fungerede det ikke men tiltrods for dette kan jeg se fordelen og forstå hvorfor brug af classes i p5.js er særdeles relevant i forhold object abstraction. 

![](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX6/kode_screenshot.png)

![](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX6/websideError_screenshot.png)


<!-- blank line -->
----
<!-- blank line -->

## Referencer

- p5.js Referencer - https://p5js.org/reference/
- p5.js inspiration - https://editor.p5js.org/Huiyi/sketches/BkaSwMVTW
- splice( ) - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice 
- font - https://www.1001fonts.com/kongtext-font.html

