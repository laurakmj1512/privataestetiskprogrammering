let myJson;
let img;
let product1;
let product2;
let product3;

let i = 0;


function preload() {
  //preloading af billederne
  img = loadImage('webshopbaggrund.png');
  product1 = loadImage("tshirt.jpg");
  product2 = loadImage("kjole.jpg");
  product3 = loadImage("bukser.jpg");
  myJson = loadJSON("jason.json");
  //preloading af font
  font = loadFont("Punk_Typewriter.otf");

}


function setup() {
  createCanvas(displayWidth, displayHeight)
  background(img);

  product1.resize(300, 400);
  product2.resize(300, 400);
  product3.resize(300, 400);

  image(product1, 350, 270);
  image(product2, 700, 270);
  image(product3, 1050, 270);

}


function draw() {
  //
  let faktaListe = myJson.voresSkyld[i].sandheden;
  let paavirkning = myJson.voresSkyld[i].paavirkning;

  //styling af tekst
  textFont(font);
  textSize(random(20, 50));
  fill(random(50, 200), 10, 20);

}


function mouseClicked() {
  //floor funktion - i værdien er altid et helt tal
  //i værdien består af en random sætning("paavirkning" fra vores jason.json fil)
  i = floor(random(0, 8))

  //text henter en sætning("paavirkning") fra listen "voresSkyld"
  //placeringen er random men sat til inden for skærmens billede 
  text(myJson.voresSkyld[i].paavirkning, random(0, 1000), random(0, 700));
}




//REFERENCELISTE

// Billeder:
// t-shirt - https://www.freepik.com/free-photo/pretty-woman-wearing-tshirt_4757209.htm#query=tshirt&position=15&from_view=search&track=sph
// kjole - https://www.freepik.com/free-photo/curly-girl-beautiful-dress_6415931.htm#query=kjole&position=30&from_view=search&track=ais 
// bukser - https://www.freepik.com/free-photo/beige-loose-pants-white-tee-women-s-fashion-closeup_15476136.htm#page=3&query=bukser&position=24&from_view=search&track=ais 

// Font:
// https://www.dafont.com/punk-typewriter.font?text=punk

// Faktasætninger anvendt i jason.json fil:
// https://www.ecolabel.dk/da/forbruger/mode-og-tekstil/noeglefacts-om-toejproduktion
// https://earth.org/fast-fashion-facts/
// https://translate.google.com/?hl=da&sl=en&tl=da&text=68%25%20of%20fast%20fashion%20brands%20don%E2%80%99t%20maintain%20gender%20equality%20at%20production%20facilities%E2%80%9D&op=translate
// https://www.panaprium.com/blogs/i/fast-fashion-facts