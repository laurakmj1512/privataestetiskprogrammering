# MiniX5: Auto-generator

_Link til min [RunMe](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX5/index.html)_

_Link til mit [kode](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX5/sketch.js)_


Det at man med et par for-loops og conditional statements kan man skabe noget kode som kan skabe kunst og visuelle flotte billeder er jo egentlig ret vildt. Jeg ønskede ikke at skabe noget der var kompliceret, men som virkelig visuelt viste at det var random auto-genereret. Jeg valgte at jeg ville skabe en random collage der skulle bestå af en række forskellige billeder.  

Jeg startede ud med at udvælge ni forskellige billeder. Jeg navngav dem 0 til 8 og indsatte dem i et array i `function preload()`. Forneden kan der ses et bilede af koden for dette. Da  `i`værdien er nul hentes det første billede som er navngivet 0.png. Derefter tilføjes der 1 til værdien `i` og billede 1.png hentes osv.

![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX5/screenshot_code1.png)

Derefter i `setup()` skriver jeg koden der skal hente billederne random, placere dem random samt bestemme hvor mange der skal placeres. Ved at oprette en knap som genindlæser `setup()` skabes der hver gang et nyt billede hver gang der klikkes på knappen.

![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX5/screenshot_code2.png)

Som baggrund har jeg blot preloadet et billede i samme kunstneriske stemning og stilart som billederne jeg har valgt. Jeg har altid fundet klassicistisk kunst interessant og i forbindelse funktionen random kunne jeg på en måde kombinere to meget forskellige ting. Det tillokkende for mig i forbindelse med klassisk kunst og klassiske skulpturer er det harmoniske, det pæne og ordentlige. Og ved at indsætte billederne random, uden nogen form for orden og billederne clasher med hinanden føler jeg at der på en måde skabes en pæn form for kaos. 

![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX5/screenshot1.png)

<!-- blank line -->
----
<!-- blank line -->

Inspirationen til denne miniX kom fra et lignende eksempel fra p5.js. Selve funktionen random() i forbindelse med kreering af kunst og billeder fandt jeg yderste sjovt og tankevækkende. Det at man aldrig får det samme billede og der hver bruger ser og oplever vil altid være anderledes og unik.  Dog under selve udførelsen af den begyndte jeg at tænke over om hvorvidt noget nogensinde kan være 100% random. Koden er sat til at vælge 1000 billeder random og placere dem random men jeg synes dog stadig at der ofte hver gang var et billede der ofte gik igen. Og dette satte gang i tankerne om noget nogensinde kan være 100% random og om uanset hvad vil der altid være en form ”pattern”. Men at det måske føles random fordi det ikke er mig selv som er i kontrol over resultatet når jeg kører min kode. 

![](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX5/Screenvideo.mov)

Jeg synes selv at der er noget spænde ved at jeg som skaber af koden ikke har kontrol over hvad koden egentlig skaber i sidste ende. Jeg ved hvad den kan skabe ud fra de betingelser som jeg som sagt har sat men grunde funktionen random er det i sidste ende ikke mig eller brugeren der skaber noget. Til trods for jeg egentlig har skrevet en tekst der opfordre brugeren til at skabe nyt kunst ved at trykke på knappen. 

Jeg finder selve konceptet auto-genereret kode særdeles spændende. For det er jo en måde man kan skabe en ”selvtænkende” maskine. Dog selvfølge reagerer den ud fra vores regelsæt. Men finder der interessant og sjovt hvad man egentlig kan skabe visuelt med et stykke auto-generende kode. 







## Referencer

- p5.js Referencer - https://p5js.org/reference/
- p5.js inspiration - https://editor.p5js.org/deibel.48/sketches/TknYctA3j

**Anvendte billeder:**

- billedramme - https://www.freepik.com/free-vector/picture-frame-sticker-home-decor-vintage-gold-design-vector_20775420.htm#page=3&query=classical%20art&position=34&from_view=search&track=ais
- Græsk krigerhovede - https://www.freepik.com/free-vector/head-with-helmet-vintage-illustration-remixed-from-artwork-by-john-flaxman_16327569.htm#page=2&query=classical%20art&position=14&from_view=search&track=ais
- blad - https://www.freepik.com/free-vector/fragment-vector-illustration-remixed-from-artworks-by-sir-edward-coley-burne-ndash-jones_17204048.htm#query=classical%20art&position=18&from_view=search&track=ais
- blomsteranordning - https://www.freepik.com/free-vector/blooming-iris-flower-vintage-vase_12459281.htm#page=4&query=classical%20art&position=17&from_view=search&track=ais
- græsk mandehovede - https://www.freepik.com/free-vector/head-youth-statue-greek-god-aesthetic-post_15078477.htm#page=2&query=classical%20art&position=4&from_view=search&track=ais
- damehovede - https://www.freepik.com/free-photo/greek-statue-engraving-style_17598786.htm#query=classical%20art&position=5&from_view=search&track=ais
- nøgne damer - https://www.freepik.com/free-vector/three-graces-vector-nude-goddess-famous-painting-remixed-from-artworks-by-raphael_17204115.htm#query=classical%20art&position=13&from_view=search&track=ais
- hænder - https://www.freepik.com/free-vector/finger-god-background-vector-aesthetic-brown-design_20346142.htm#query=classical%20art&position=25&from_view=search&track=ais
- havfrue - https://www.freepik.com/free-vector/sea-nymph-vector-illustration-remixed-from-artworks-by-sir-edward-coley-burne-ndash-jones_17204055.htm#query=classical%20art&position=33&from_view=search&track=ais
- baggrund - https://www.freepik.com/free-photo/abstract-oil-paint-textured-background_25267397.htm?query=classical
- font - https://www.1001fonts.com/victoriantext-font.html
