
let button;

var a = 50;
//var b = 30;
var c = 10;
var d = 190;
var e = 100;
var f = 100;
var pressed = false;


var r;
var g;
var b;
var rr;
var gg;
var bb;

//variabel for slider hud
var rSliderH = 0;
var gSliderH = 0;
var bSliderH = 0;
//variabel for slider øjne
var rSliderE = 0;
var gSliderE = 0;
var bSliderE = 0;

function preload() {
  img = loadImage("phone_illustration.png");
  imgB = loadImage("background.png");
  infoFont = loadFont("../miniX7/Righteous-Regular.ttf");
}
function setup() {
  // put setup code here
  createCanvas(windowWidth, windowHeight)


  // knapstyling
  button = createButton("Shake me!");
  
  button.position(125, 410);
  button.size(150, 80);
  button.addClass("buttonStyling");
  button.style("font-size", "20px");
  button.style("color", "#ffffff")
  
  

  button.mousePressed(() => pressed = true)
  button.mouseReleased(() => pressed = false);

  // slider for hudfarve
  rSliderH = createSlider(0, 255, 172);
  gSliderH = createSlider(0, 255, 122);
  bSliderH = createSlider(0, 255, 73);

  rSliderH.position(120, 216);
  rSliderH.style("width", "100px");
  gSliderH.position(120, 236);
  gSliderH.style("width", "100px");
  bSliderH.position(120, 256);
  bSliderH.style("width", "100px");

  // slider for øjne
  rSliderE = createSlider(0, 255, 56);
  gSliderE = createSlider(0, 255, 40);
  bSliderE = createSlider(0, 255, 24);

  rSliderE.position(120, 306);
  rSliderE.style("width", "100px");
  rSliderE.addClass("sliderStyling");
  gSliderE.position(120, 326);
  gSliderE.style("width", "100px");
  bSliderE.position(120, 346);
  bSliderE.style("width", "100px");
}

function emoji(x, y, f) {
  //tekstboks
  fill(160, 190, 220, 200);
  rect(200, 295, 295, 500, 30);
  fill(0);
  textFont(infoFont);
  textSize(25);
  text("Hvad er den rigtige?", 80, 100);
  text("emoji for dig?", 80, 125);

  textSize(15);
  text("Design din personlige emoji så den", 80, 160);
  text("passer til dig og hvem du er.", 80, 180);

  text("Tilpas din hudfarve", 80, 210);
  push();
  textSize(12)
  text("Rød", 81, 230);
  text("Grøn", 81, 250);
  text("Blå", 81, 270);
  pop();

  text("Tilpas din øjenfarve", 80, 300);
  push();
  textSize(12)
  text("Rød", 81, 320);
  text("Grøn", 81, 340);
  text("Blå", 81, 360);
  pop();


  //slider for hudfarve
  var r = rSliderH.value();
  var g = gSliderH.value();
  var b = bSliderH.value();

  //slider for øjenfarve
  var rr = rSliderE.value();
  var gg = gSliderE.value();
  var bb = bSliderE.value();

  //hovede
  fill(r, g, b);
  noStroke();
  smooth();
  circle(x, y, d);
  rectMode(CENTER);

  //øjne
  //det hvide i øjnene
  fill(255);
  circle(x - 40, y - 20, a);
  circle(x + 40, y - 20, a);
  //iris
  fill(rr, gg, bb);
  circle(x - 40, y - 20, 20);
  circle(x + 40, y - 20, 20);
  //pupil
  fill(10);
  circle(x - 40, y - 20, c);
  circle(x + 40, y - 20, c);

  //mund
  fill(0)
  rect(x, y + 30, e, 20, c, c, e, e);
  arc(x, y + 30, f, f, 0, PI)
}

function draw() {
  background(imgB);

  push();
  imageMode(CENTER);
  image(img, windowWidth / 2, windowHeight * 0.65);
  pop();

  console.log(pressed);
  if (pressed) {
    emoji(random(width / 2 - 10, width / 2 + 10), random(height / 2 - 10, height / 2 + 10), 0);
  }
  else {
    emoji(width / 2, height / 2, 100);
  }


}
