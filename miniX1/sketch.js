//overordnede definition af variablerne r, g, b 
let r1, g1, b1; 
function setup() {
  // put setup code here
  createCanvas (displayWidth, displayHeight)
  //startværdien af r1, g1, b1 - længere nede defineres startbaggrunden
  r1=245;
  g1=245;
  b1=220;
  
}



function draw() {
  // put drawing code here
  background(r1,g1,b1);

  let a = color(204, 228, 237);
  fill(a);
  noStroke();
  circle(400, 120, 100);
  a = color(237,168,182);
  fill(a);
  circle(400, 370, 100);
  a = color(193,237,225);
  fill(a);
  circle(400, 600, 100);


  let b = color(237, 204, 168);
  fill(b);
  noStroke();
  square(700, 70, 100);
  b = color(180,237,190);
  fill(b);
  square(700, 320, 100);
  b = color(195,177,240);
  fill(b);
  square(700, 550, 100);


  let c = color(240, 224, 125);
  fill(c);
  noStroke();
  triangle(1100, 80, 1050, 180, 1150, 180);
  c = color(237,215,213);
  fill(c);
  triangle(1100, 330, 1050, 430, 1150, 430);
  c = color(178,237,233);
  fill(c);  
  triangle(1100, 550, 1050, 650, 1150, 650);

}

//når musen trykkes ændres bagrundsfarven - værdierne r1, g1 og b1 får en ny værdi
function mousePressed () {
  r1 = random(255)
  g1 = random(255)
  b1 = random(255)
}

